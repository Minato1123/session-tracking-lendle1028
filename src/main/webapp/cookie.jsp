<%-- 
    Document   : cookie
    Created on : Sep 13, 2018, 9:19:51 PM
    Author     : lendle
--%>

<%@page import="java.net.URLDecoder"%>
<%@page import="java.net.URLEncoder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            body {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                height: 100vh;
            }
            
            .btn-submit {
                width: 100%;
                margin-top: 1rem;
            }
        </style>
    </head>
    <body>
        <h1>Hello World!</h1>
        <% // java
            String food="";
            Cookie [] cookies = request.getCookies();
            for(Cookie cookie : cookies){
                if(cookie.getName().equals("food")){
                    food = URLDecoder.decode(cookie.getValue(), "utf-8"); // cookie 內容，有編碼就要有解碼
                    break;
                }
            }
        %>
        <form action="saveCookie" method="POST">
            Your Favorite Food: <input type="text" name="food" value="<%=food%>"/><br/>
            <input class="btn-submit" type="submit"/>
        </form>
    </body>
</html>
